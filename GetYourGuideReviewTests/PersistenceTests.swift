//
//  PersistenceTests.swift
//  GetYourGuideReviewTests
//
//  Created by Chris on 19.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import XCTest

@testable import GetYourGuideReview

class PersistenceTests: XCTestCase {
    
    lazy var dependencies  =  TestDependencies.int(test: self)
    
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPersistReview() {
        let primaryKey = UUID.init().uuidString.hashValue
        let reviewTitle = "persistTest"
        
        let review = ReviewModel()
        
        try? dependencies.persistenceService.write{realm in
            review.reviewId = primaryKey
            review.title = reviewTitle
            realm.add(review)
        }
        
        try? dependencies.persistenceService.read{ realm in
            guard let review = realm.object(ofType: ReviewModel.self, forPrimaryKey: primaryKey) else {
                XCTFail("could not retrieve stored review")
                return
            }
            XCTAssertEqual(review.title, reviewTitle)
        }
        
    }
}

//
//  Stubs.swift
//  GetYourGuideReviewTests
//
//  Created by Chris on 19.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import RealmSwift
import XCTest
import Moya
import Reachability
import RxSwift

@testable import GetYourGuideReview

fileprivate struct InMemoryRealmProvider: RealmProviderType {
    
    
    func realm() throws -> Realm {
        let configuration = Realm.Configuration(inMemoryIdentifier: inMemoryIdentifier)
        return try Realm(configuration: configuration)
    }
    
    let inMemoryIdentifier: String
    
    
    init(identifier: String){
        inMemoryIdentifier = identifier
    }
    
}
enum ReachabilityMock {
    case alwaysConnected
    case neverConnected
}

extension ReachabilityMock: ReachabilityType{
    var status: Observable<Reachability.Connection> {
        switch self {
        case .neverConnected:
            return .just(.none)
        case .alwaysConnected:
            return .just(.wifi)
        }
    }
    
    
}


struct TestDependencies: HasPersistenceService, HasReviewsAPI, HasReachability, HasReviewsCache {
    var reviewsCache: ReviewCacheType
    var reachability: ReachabilityType
    var reviewsAPI: MoyaProvider<ReviewsAPI>
    var persistenceService: PersistenceServiceType

    
    private init(realmProvider: RealmProviderType){
        persistenceService = PersistenceService(realmProvider: realmProvider)
        self.reviewsCache = ReviewsCache(persistenceService: persistenceService)
        self.reachability = ReachabilityMock.alwaysConnected
        reviewsAPI = MoyaProvider<ReviewsAPI>.init( stubClosure: { server -> StubBehavior in
            return StubBehavior.immediate
        })
        
        
    }
    
}

extension TestDependencies {
    static func int(test: XCTestCase) -> TestDependencies{
        let realmProvider = InMemoryRealmProvider(identifier: test.name )
        return TestDependencies(realmProvider: realmProvider)
    }
}

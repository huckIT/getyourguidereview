//
//  ResponseDecoding.swift
//  GetYourGuideReviewTests
//
//  Created by Chris on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import XCTest
import RxSwift
import RxBlocking
import Moya
@testable import GetYourGuideReview

class GetYourGuideReviewResponseDecodingTests: XCTestCase {
    
    let mockResponse =   "{\"status\":true,\"total_reviews_comments\":501,\"data\":[{\"review_id\":2840117,\"rating\":\"5.0\",\"title\":\"Super informativ\",\"message\":\"Sehr nette und kompetente F\\u00fchrung.\",\"author\":\"Kerstin \\u2013 Germany\",\"foreignLanguage\":true,\"date\":\"June 17, 2018\",\"date_unformatted\":{},\"languageCode\":\"de\",\"traveler_type\":\"friends\",\"reviewerName\":\"Kerstin\",\"reviewerCountry\":\"Germany\"},{\"review_id\":2837792,\"rating\":\"5.0\",\"title\":\"Kompetente und kurzweilige F\\u00fchrung mit Andreas und vielen Infos\",\"message\":\"\",\"author\":\"a GetYourGuide Customer \\u2013 Germany\",\"foreignLanguage\":true,\"date\":\"June 17, 2018\",\"date_unformatted\":{},\"languageCode\":\"de\",\"traveler_type\":\"solo\",\"reviewerName\":\"Rebecca\",\"reviewerCountry\":\"Germany\"},{\"review_id\":2837249,\"rating\":\"5.0\",\"title\":\"Super interessant und kurzweilig.\",\"message\":\"\",\"author\":\"Stefan \\u2013 Germany\",\"foreignLanguage\":true,\"date\":\"June 17, 2018\",\"date_unformatted\":{},\"languageCode\":\"de\",\"traveler_type\":null,\"reviewerName\":\"Stefan\",\"reviewerCountry\":\"Germany\"},{\"review_id\":2800542,\"rating\":\"4.0\",\"title\":\"Interesting but short\",\"message\":\"\",\"author\":\"Ameyalli \\u2013 Germany\",\"foreignLanguage\":false,\"date\":\"June 14, 2018\",\"date_unformatted\":{},\"languageCode\":\"en\",\"traveler_type\":null,\"reviewerName\":\"Ameyalli\",\"reviewerCountry\":\"Germany\"},{\"review_id\":2793819,\"rating\":\"5.0\",\"title\":\"Sehr interessant, wir hatten einen super Guide.\",\"message\":\"Auf Empfehlung haben wir an dieser Tour teilgenommen. Es hat unsere Erwartungen \\u00fcbertroffen. Sehr angenehm war die gr\\u00f6\\u00dfte der Gruppe, dadurch bekam man viel geboren. Die zweite angebotene Tour steht schon auf dem Plan.\",\"author\":\"Sabine \\u2013 Germany\",\"foreignLanguage\":true,\"date\":\"June 13, 2018\",\"date_unformatted\":{},\"languageCode\":\"de\",\"traveler_type\":\"couple\",\"reviewerName\":\"Sabine\",\"reviewerCountry\":\"Germany\"}]}"
    
    
    //todo: replace this one by the API one
    let decoder = JSONDecoder()
    
    var utf8EncodedMockResponse: Data{
        
        //let wI = NSMutableString( string: mockResponse)
        //CFStringTransform( wI, nil, "Any-Hex/Java" as NSString, true )
        return (mockResponse).data(using: .utf8)!
    }
    
    override func setUp() {
        super.setUp()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.iso8601Extended)
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    //TODO: extend by checking the right dates ( not only nil-checking)
    func testDateFormatting(){
        let validDates = ["June 17, 2018","June 1, 2018"]
        let formatter = DateFormatter.iso8601Extended
        for valid in validDates{
            let date = formatter.date(from: valid)
            XCTAssertNotNil(date)
        }
    }
    
    func testJSONValidity(){
        do{
            let jsonObject = try JSONSerialization.jsonObject(with: utf8EncodedMockResponse, options: .allowFragments)
            if !JSONSerialization.isValidJSONObject(jsonObject) {
                XCTFail("invalid mock response")
            }
        }
        catch {
            XCTFail(error.localizedDescription)
        }
    }
    
    
    func testReviewsResponse() {
        do{
            let decoded = try decoder.decode(ReviewResponse.self, from: utf8EncodedMockResponse )
            //check basic response
            XCTAssertEqual(decoded.status, true)
            XCTAssertEqual(decoded.totalReviews , 501)
            XCTAssertEqual(5, decoded.items.count)
            //check first review
            guard let firstReview = decoded.items.first else { XCTFail(); return }
            XCTAssertEqual(firstReview.author, "Kerstin \u{2013} Germany")
            
        }
        catch{
            XCTFail(error.localizedDescription)
        }
    }
    
    func validateSingleReview(review: String){
        do{
            let decoded = try decoder.decode(Review.self, from:review.data(using: .utf8)! )
            XCTAssertNotNil(decoded)
        }
        catch{
            XCTFail(error.localizedDescription+" occured on : \(review)")
        }
    }
    //todo: change or remove
    func testSomeReviews(){
        let reviews  = "{\"review_id\":2847548,\"rating\":\"4.0\",\"title\":\"Ort mit viel Geschichte\",\"message\":\"Die 2-st\\u00fcndige F\\u00fchrung war sehr interessant und hat uns einen tollen Gesamteindruck des Flughafens n\\u00e4hergebracht. Wer h\\u00e4tte gedacht, dass Tempelhof ein so geschichtstr\\u00e4chtiger Ort ist? Wir haben zwar nur einen kleinen Teil des Geb\\u00e4udes mit der F\\u00fchrung besichtigen k\\u00f6nnen, aber dort schien die Zeit stehen geblieben zu sein. Kein Wunder also, dass schon einige Hollywood-Verfilmungen dort stattfanden. Ich kann einen Besuch nur empfehlen!\",\"author\":\"a GetYourGuide Customer \\u2013 Germany\",\"foreignLanguage\":true,\"date\":\"June 18, 2018\",\"date_unformatted\":{},\"languageCode\":\"de\",\"traveler_type\":\"friends\",\"reviewerName\":\"Alicia\",\"reviewerCountry\":\"Germany\"},{\"review_id\":2840117,\"rating\":\"5.0\",\"title\":\"Super informativ\",\"message\":\"Sehr nette und kompetente F\\u00fchrung.\",\"author\":\"Kerstin \\u2013 Germany\",\"foreignLanguage\":true,\"date\":\"June 17, 2018\",\"date_unformatted\":{},\"languageCode\":\"de\",\"traveler_type\":\"friends\",\"reviewerName\":\"Kerstin\",\"reviewerCountry\":\"Germany\"},{\"review_id\":2837792,\"rating\":\"5.0\",\"title\":\"Kompetente und kurzweilige F\\u00fchrung mit Andreas und vielen Infos\",\"message\":\"\",\"author\":\"a GetYourGuide Customer \\u2013 Germany\",\"foreignLanguage\":true,\"date\":\"June 17, 2018\",\"date_unformatted\":{},\"languageCode\":\"de\",\"traveler_type\":\"solo\",\"reviewerName\":\"Rebecca\",\"reviewerCountry\":\"Germany\"},{\"review_id\":2837249,\"rating\":\"5.0\",\"title\":\"Super interessant und kurzweilig.\",\"message\":\"\",\"author\":\"Stefan \\u2013 Germany\",\"foreignLanguage\":true,\"date\":\"June 17, 2018\",\"date_unformatted\":{},\"languageCode\":\"de\",\"traveler_type\":null,\"reviewerName\":\"Stefan\",\"reviewerCountry\":\"Germany\"},{\"review_id\":2800542,\"rating\":\"4.0\",\"title\":\"Interesting but short\",\"message\":\"\",\"author\":\"Ameyalli \\u2013 Germany\",\"foreignLanguage\":false,\"date\":\"June 14, 2018\",\"date_unformatted\":{},\"languageCode\":\"en\",\"traveler_type\":null,\"reviewerName\":\"Ameyalli\",\"reviewerCountry\":\"Germany\"},{\"review_id\":2793819,\"rating\":\"5.0\",\"title\":\"Sehr interessant, wir hatten einen super Guide.\",\"message\":\"Auf Empfehlung haben wir an dieser Tour teilgenommen. Es hat unsere Erwartungen \\u00fcbertroffen. Sehr angenehm war die gr\\u00f6\\u00dfte der Gruppe, dadurch bekam man viel geboren. Die zweite angebotene Tour steht schon auf dem Plan.\",\"author\":\"Sabine \\u2013 Germany\",\"foreignLanguage\":true,\"date\":\"June 13, 2018\",\"date_unformatted\":{},\"languageCode\":\"de\",\"traveler_type\":\"couple\",\"reviewerName\":\"Sabine\",\"reviewerCountry\":\"Germany\"},{\"review_id\":2754530,\"rating\":\"4.0\",\"title\":\"Perfect guide and spectacular place\",\"message\":\"\",\"author\":\"J\\u00e9r\\u00e9mie \\u2013 France\",\"foreignLanguage\":false,\"date\":\"June 7, 2018\",\"date_unformatted\":{},\"languageCode\":\"en\",\"traveler_type\":\"couple\",\"reviewerName\":\"J\\u00e9r\\u00e9mie\",\"reviewerCountry\":\"France\"},{\"review_id\":2722283,\"rating\":\"4.0\",\"title\":\"Interessant!\",\"message\":\"Heel indrukwekkend. Goeie gids die met enthousiasme vertelt en de luchthaven is mooi om te zien.\",\"author\":\"Patricia \\u2013 Netherlands\",\"foreignLanguage\":true,\"date\":\"June 3, 2018\",\"date_unformatted\":{},\"languageCode\":\"nl\",\"traveler_type\":\"solo\",\"reviewerName\":\"Patricia\",\"reviewerCountry\":\"Netherlands\"},{\"review_id\":2720468,\"rating\":\"4.0\",\"title\":null,\"message\":\"Walking around the airport, almost freely. Better understanding the meaning of the airport to Berlin. A lot of stairs.\",\"author\":\"Mariana \\u2013 Germany\",\"foreignLanguage\":false,\"date\":\"June 3, 2018\",\"date_unformatted\":{},\"languageCode\":\"en\",\"traveler_type\":null,\"reviewerName\":\"Mariana\",\"reviewerCountry\":\"Germany\"},{\"review_id\":2714966,\"rating\":\"5.0\",\"title\":\"Excellent tour guide was very informative fascinating building\",\"message\":\"\",\"author\":\"Darron \\u2013 United Kingdom\",\"foreignLanguage\":false,\"date\":\"June 3, 2018\",\"date_unformatted\":{},\"languageCode\":\"en\",\"traveler_type\":\"couple\",\"reviewerName\":\"Darron\",\"reviewerCountry\":\"United Kingdom\"}"
        let components = reviews.components(separatedBy: "},{")
        
        let reviewStrings = components.map{ str -> String in
            var result = ""
            if str.first != "{" { result.append("{") }
            result.append(str)
            if str.last != "}" { result.append("}") }
            return result
        }
        
        for review in reviewStrings {
            validateSingleReview(review: review)
        }
        
        
    }
    func testDecodeSingleReview(){
        let mockReview = "{\"review_id\":2840117,\"rating\":\"5.0\",\"title\":\"Super informativ\",\"message\":\"Sehr nette und kompetente F\\u00fchrung.\",\"author\":\"Kerstin \\u2013 Germany\",\"foreignLanguage\":true,\"date\":\"June 17, 2018\",\"date_unformatted\":{},\"languageCode\":\"de\",\"traveler_type\":\"friends\",\"reviewerName\":\"Kerstin\",\"reviewerCountry\":\"Germany\"}"
        validateSingleReview(review: mockReview)
    }
    
    
    func testDecodeIncompleteReview(){
        //traveler type is null
        let mockReview = "{\"review_id\":2837249,\"rating\":\"5.0\",\"title\":\"Super interessant und kurzweilig.\",\"message\":\"\",\"author\":\"Stefan \\u2013 Germany\",\"foreignLanguage\":true,\"date\":\"June 17, 2018\",\"date_unformatted\":{},\"languageCode\":\"de\",\"traveler_type\":null,\"reviewerName\":\"Stefan\",\"reviewerCountry\":\"Germany\"}".data(using: .utf8)!
        do{
            let decoded = try decoder.decode(Review.self, from: mockReview )
            XCTAssertNotNil(decoded)
            XCTAssertEqual(decoded.author, "Stefan \u{2013} Germany")
        }
        catch{
            XCTFail(error.localizedDescription)
        }
    }
    
    func testObservableDecoding(){
        let provider = MoyaProvider<ReviewsAPI>.init( stubClosure: { server -> StubBehavior in
            return StubBehavior.immediate
        })        
        let params = FetchReviewsRequest(activityId: "stub")
        do{
            let request =  provider.rx.request(ReviewsAPI.fetchReviews(request: params)).asObservable()
            
            let mappedRequest = request.mapDecodable(ReviewResponse.self).catchError{ error -> Observable<ReviewResponse> in
                XCTFail("Faild to map ReviewsResponse: \(error.localizedDescription)")
                return Observable.empty()
            }
                
            guard let result = try mappedRequest.toBlocking().first() else {  XCTFail("mapped element is nil") ; return}
            XCTAssertTrue(result.status)
        }
        catch {
            XCTFail("Faild to retrieve observable event: \(error.localizedDescription)")
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}



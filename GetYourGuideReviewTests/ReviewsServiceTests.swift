//
//  ReviewsServiceTests.swift
//  GetYourGuideReviewTests
//
//  Created by Chris on 19.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import XCTest
import RxBlocking
import RxSwift

@testable import GetYourGuideReview

class ReviewsServicetests: XCTestCase {
    
    lazy var dependencies  =  TestDependencies.int(test: self)
    
    lazy var service: ReviewsService = ReviewsService(dependencies: self.dependencies)
    
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFetchReviews() {
        let params = FetchReviewsRequest(activityId: "test")
        do{
            let reviews = try service.fetchReviews(for: params).toBlocking().first()
            XCTAssertNotNil(reviews)
            let firstReview = reviews?.reviews.first
            XCTAssertNotNil(firstReview)
        }
        catch {
            XCTFail(error.localizedDescription)
        }
    }
}

//
//  RealmProvider.swift
//  GetYourGuideReview
//
//  Created by Chris on 19.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import RealmSwift

protocol RealmProviderType{
     func realm() throws -> Realm
}

struct RealmProvider: RealmProviderType{
    func realm() throws -> Realm {
        //This gives room for more advanced Realm setup later on, e.g. syncConfiguration or multiple Realms
        return try Realm()
    }
}

//
//  UserProvider.swift
//  GetYourGuideReview
//
//  Created by Chris on 20.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation

struct UserProfile {
    let country: String
    //TODO: this could also be inferred from iOS settings
    let languageCode: String
    let username: String
    let travelerType: TravelerType
}


protocol UserProviderType {
    var loggedInUserProfile: UserProfile {get}
}
protocol HasUserProvider {
    var userProvider: UserProviderType {get}
}
struct UserProviderMock: UserProviderType {
    var loggedInUserProfile: UserProfile {
        return UserProfile(country: "de", languageCode: "de", username: "Chris", travelerType: .couple)
    }
}

//
//  ReviewsViewController.swift
//  GetYourGuideReview
//
//  Created by Chris on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import UIKit
import RxDataSources
import RxCocoa
import RxSwift






class ReviewsViewController: UIViewController, BindableType{
    
    var viewModel: ReviewsViewModel!
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var composeButton: UIBarButtonItem!
    
    
    lazy var dataSource = RxTableViewSectionedReloadDataSource<ReviewsSectionModel>(configureCell: { (ds, tv, ip, model) -> UITableViewCell in
        guard let cell = tv.dequeueReusableCell(withIdentifier: model.reuseIdentifier)  else {
            #if DEBUG
                fatalError("Failed to dequeue ReviewCell with ReuseIdentifier \(ReviewCell.reuseIdentifier)")
            #endif
            return UITableViewCell()
        }
        switch model{
        case .review(review: let review):
            (cell as? ReviewCell)?.configure(review: review)
            return cell;
        case .activity(let activity):
            (cell as? ActivityCell)?.configure(activity: activity)
            return cell
        default:
            return cell;
        }
        
    })
    
    
    func bindViewModel() {
        //Data Source
        viewModel.reviewSections.bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        bindLoadMore()
        //compose action
        composeButton.rx.action = viewModel.compose        
    }
    
    func bindLoadMore(){
        //after first scroll
        let firstScroll = tableView.rx.didScroll.take(1)
        
        let lastSectionReached = tableView.rx.willDisplayCell.filter{ [weak self] (event: WillDisplayCellEvent) in
            //when showing last section
            guard let sectionCount = self?.dataSource.sectionModels.count else { return false }            
            return (sectionCount - 1 )   == event.indexPath.section
            }.map{ _ in ()}
        Observable.concat(firstScroll,lastSectionReached).bind(to: viewModel.loadNextPageAction.inputs).disposed(by: disposeBag)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}



//
//  ReviewCell.swift
//  GetYourGuideReview
//
//  Created by Christian Huck on 22.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import UIKit


class ReviewCell: UITableViewCell{
    static let reuseIdentifier = "reviewCell"
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var starRatingViewContainer: XibView!
    
    var starRatingView: StarRatingView {
        return starRatingViewContainer.contentView as! StarRatingView
    }
}

//
//  StartRatingView.swift
//  GetYourGuideReview
//
//  Created by Christian Huck on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import  UIKit
import RxGesture
import RxSwift
import RxSwiftExt
import RxCocoa



@IBDesignable
class StarRatingView: UIStackView {
    
    @IBOutlet var stars: [UIImageView]!
    
    let disposeBag = DisposeBag()
    
    var  appBundle :  Bundle {
        return Bundle(for: type(of: self))
    }
    
    lazy var starOn = UIImage(named: "star_on", in: appBundle, compatibleWith: self.traitCollection)
    lazy var starOff = UIImage(named: "star_off", in: appBundle, compatibleWith: self.traitCollection)
    
    
    //TODO: refactor to Rx ControlProperty (rx.rating)
    public let rxRating = PublishSubject<Int>()
    func renderRating(rating: Int?){
        
        let rating = rating ?? 3
        if rating < 0 || rating > 5 { return }
        for (i,iv) in stars.enumerated(){
            iv.image = (i < rating) ? starOn :  starOff
            iv.setNeedsDisplay()
        }
    }
    
    var rating: Int? = nil {
        didSet{
            UIView.animate(withDuration: 0.5) {
                self.renderRating(rating: self.rating)
            }
            guard let rating = rating else {return}
            self.rxRating.onNext(rating)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.renderRating(rating: rating)
    }
    
    override func didMoveToWindow(){
        Observable.merge(rx.tapGesture().asLocation(in: .view),rx.panGesture().asLocation(in: .view)).filter{[weak self] _ in return self?.isUserInteractionEnabled ?? false}.map { [weak self] location  -> Float? in
            guard let this = self else {return nil}
            //in % of view width
            return Float(location.x/this.bounds.width)
            }.unwrap().subscribe(onNext: { [weak self] fraction in
                let ratingOfFive = 5 * fraction
                self?.rating = Int(round(ratingOfFive))
            }).disposed(by: disposeBag)
        
    }
    
    
}


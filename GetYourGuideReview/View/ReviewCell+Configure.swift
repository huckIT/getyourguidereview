//
//  ReviewCell+Configure.swift
//  GetYourGuideReview
//
//  Created by Chris on 19.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import UIKit
extension ReviewCell{
    func configure(review: ReviewType){
        title.text = review.title
        message.text = review.message
        let intRating = Int(round(Float(review.rating) ?? 1))
        starRatingView.rating = intRating
        authorLabel.text = review.author
        starRatingView.renderRating(rating: intRating)
    }
    
    override func prepareForReuse() {
        title.text = nil
        message.text = nil
        starRatingView.rating = 0
    }
}



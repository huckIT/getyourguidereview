//
//  ActivityCell.swift
//  GetYourGuideReview
//
//  Created by Christian Huck on 20.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import UIKit

protocol ActivityType{
    var activityId: String {get}
    var title: String {get}
    var coverImage: UIImage {get}
}
extension ActivityCell{
    func configure(activity: ActivityType){
        title.text = activity.title
        cover.image = activity.coverImage
    }
}

class ActivityCell: UITableViewCell{
    static let reuseIdentifier = "activityCell"
    @IBOutlet weak var cover: UIImageView!
    @IBOutlet weak var title: UILabel!
}


//
//  ComposeReviewViewController.swift
//  GetYourGuideReview
//
//  Created by Chris on 20.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import UIKit
import RxKeyboard
import RxSwift
import RxCocoa
import RxSwiftExt
import RxGesture

class ComposeReviewViewController: UIViewController, BindableType{
    
    
    @IBOutlet weak var postButton: UIBarButtonItem!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var starRatingViewContainer: XibView!
    
    var starRatingView: StarRatingView {
        return starRatingViewContainer.contentView as! StarRatingView
    }
    
    let disposeBag = DisposeBag()
    
    var viewModel: ComposeReviewViewModel!
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var messageField: UITextView!
    
    func setupRx(){
        //Keyboard display <--> View Size
        RxKeyboard.instance.visibleHeight
            .drive(onNext: { [weak self] keyboardVisibleHeight in
                self?.bottomConstraint.constant =  keyboardVisibleHeight
            })
            .disposed(by: disposeBag)
        
        self.view.rx.tapGesture().subscribe(){ [weak self] _ in
            self?.titleField.resignFirstResponder()
            self?.messageField.resignFirstResponder()
            }.disposed(by: disposeBag)
    }
    
    func bindViewModel() {
        //Input fields
        starRatingView.rxRating.bind(to:viewModel.rating).disposed(by: disposeBag)
        titleField.rx.text.unwrap().bind(to:viewModel.title).disposed(by: disposeBag)
        messageField.rx.text.unwrap().bind(to:viewModel.message).disposed(by: disposeBag)
        //Buttons
        cancelButton.rx.action = viewModel.dismiss
        postButton.rx.action = viewModel.postAction                
        viewModel.uiNotification.do(onNext:{ [weak self] _ in
            self?.view.hideAllToasts()
        }).bind(to:self.view.rx.toasts).disposed(by: disposeBag)
        viewModel.setupRx()
    }
    
    override func viewDidLoad() {
        setupRx()
    }
    
    
}

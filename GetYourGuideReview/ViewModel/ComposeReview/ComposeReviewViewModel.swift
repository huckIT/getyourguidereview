//
//  ComposeReviewViewModel.swift
//  GetYourGuideReview
//
//  Created by Chris on 20.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import Action
import RxSwift
import RxSwiftExt

class ComposeReviewViewModel{
    
    //Output
    let dismiss: CocoaAction
    public var uiNotification: Observable<Toasty>{
        return _uiNotifications.share(replay:1,scope:.forever)
    }
    private var _uiNotifications = ReplaySubject<Toasty>.create(bufferSize: 1)
    var isValid = BehaviorSubject<Bool>(value:false)
    
    //Input
    let title = BehaviorSubject<String>(value:"")
    let message = BehaviorSubject<String>(value:"")
    let rating = BehaviorSubject<Int>(value:2)
    
    
    typealias Dependencies = HasReviewsService & HasUserProvider
    
    
    lazy var postAction = CocoaAction.init(enabledIf: self.isValid){ _ in
        return  Observable.just(())
    }
    
    
    
    //RX
    let disposeBag = DisposeBag()
    func setupRx(){
        
        // Response validation
        let reviewObservable = Observable
            .combineLatest(title.distinctUntilChanged(),message.distinctUntilChanged(),rating.distinctUntilChanged())
            .map{[weak self] title,message,rating -> Review? in
                guard let this = self else { return nil }
                let user  = this.userProvider.loggedInUserProfile
                let review = Review(reviewId: 0, rating: String(rating), title: title, message: message, author: user.username, foreignLanguage: true, date: Date(), languageCode: user.languageCode, travelerTypeId: user.travelerType.rawValue, reviewerName: user.username, reviewerCountry: user.country)
                return review
        }
        
        let validation = reviewObservable
            .unwrap()
            .map{$0.validateForSubmission()}
            .share(replay: 1, scope: .forever)
       
        
        validation
            .debounce(1.0, scheduler: MainScheduler.instance)
            .map{$0.materialize().errors()}
            .switchLatest().map { error -> String? in
                guard let error = error as? ReviewValidationError else { return  nil}
                switch error{
                case .NeedsRating:
                    return "Please tell us what you think - we let you touch the stars"
                case .TitleOrMessageNeeded:
                    return "why did you rate us that many stars?"
                case .TitleTooShort(let missing):
                    return  "\(missing) characters remaining for the title"
                case .MessageTooShort(let missing):
                    return "\(missing) characters remaining for the message"
                }
            }.unwrap()
            .map{ Toasty.warn(message: $0) }
            .bind(to:self._uiNotifications)
            .disposed(by: disposeBag)
        
        validation
            .map{ $0.catchErrorJustReturn(false) }
            .switchLatest()
            .subscribe(self.isValid)
            .disposed(by: disposeBag)
        
        
        
        //Publishing
        
        let publishing = postAction.executionObservables.switchLatest().withLatestFrom(reviewObservable).unwrap().map{ [weak self] review -> Observable<PublishReviewResult>? in
            guard let this = self else {return nil}
            return this.reviewsService.publish(review: review, for: this.activity.activityId)}
            .unwrap()
            .share(replay: 1, scope: .forever)
        
        let materializedPublishing = publishing
            .map{ $0.materialize()}
            .switchLatest()
        
        let failedPublishing = Observable.merge(materializedPublishing.errors()
            .map{ $0.localizedDescription},
            materializedPublishing.elements()
            .filter{!$0}.map{ _ in "An unknown Server error occured" })
        
        let successfulPublishing = materializedPublishing
            .elements()
            .filter{$0}.share(replay: 1, scope: .forever)
        
        successfulPublishing
            //give the user some time to read the success notification
            .delay(2, scheduler: MainScheduler.instance)
            .map{ _ in ()}
            .subscribe(self.dismiss.inputs)
            .disposed(by: disposeBag)
        
        //Toasties
        let validationToasts = isValid.filter{$0}.take(1).map{ _ in Toasty.positive(message: "You're good to submit - or wanna tell us more?") }
        
        let failedPublishingToasts = failedPublishing.map{ Toasty.error(message: $0) }
        
        
        let successfulPublishingToasts = successfulPublishing.map{ _ in Toasty.positive(message: "Thank you. Your review will be published shortly!") }
        
        Observable.merge(validationToasts,failedPublishingToasts,successfulPublishingToasts)
            .observeOn(MainScheduler.instance)
            .bind(to: self._uiNotifications)
            .disposed(by:disposeBag)
        
    }
    
    
    //init
    private let reviewsService: ReviewsServiceType
    private let activity: ActivityType
    private let userProvider: UserProviderType
    
    
    required init(dependencies: Dependencies,activity: ActivityType,dismiss: CocoaAction){
        reviewsService = dependencies.reviewsService
        self.activity = activity
        self.dismiss = dismiss
        self.userProvider = dependencies.userProvider        
    }
    
    
}

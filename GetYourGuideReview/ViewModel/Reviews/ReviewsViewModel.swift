//
//  ReviewsViewModel.swift
//  GetYourGuideReview
//
//  Created by Chris on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import RxSwift
import RxDataSources
import Action

class  ReviewsViewModel {
    
    typealias Dependencies  = HasReviewsService

    
    //Input
    public lazy var loadNextPageAction = Action<Void,FetchReviewsResult>{ [weak self] _ in
        guard let this = self else { return .empty() }        
        return Observable.just(()).withLatestFrom(this._reviewResults).switchLatest().map{ this.reviewsService.fetchNextPage(for: $0)}.switchLatest().take(1)
    }
    
    let compose: CocoaAction
    
    //output
    var reviewSections: Observable<[ReviewsSectionModel]> {
        
        let activityModel =  Observable.just(ReviewsSectionModel.activity(activity: self.activity))
        
        
        let reviews = _reviewResults.switchLatest().map { result -> ReviewsSectionModel in
            let items = result.reviews.map{ ReviewsSectionItem.review(review: $0) }
            return .reviews(items: items)
            } .asObservable()
        
        let loading = Observable.just(ReviewsSectionModel.loadingIndicator)
        
        
        return Observable.combineLatest([activityModel,reviews,loading])
    }
    
    //internal
    private let _reviewResults = BehaviorSubject<Observable<FetchReviewsResult>>(value: .empty())
    
    private lazy var loadNextPage = Action<FetchReviewsResult,FetchReviewsResult> { [weak self] currentResult in
        guard let this = self else { return .empty() }
        return this.reviewsService.fetchNextPage(for: currentResult)
    }
            
    private func pushReviews(_ result: Observable<FetchReviewsResult>){
        _reviewResults.onNext(result)
    }
    
    
    //RX
    let disposeBag = DisposeBag()
    
    func setupRx(){
        let params = FetchReviewsRequest(activityId: activity.activityId)
        let reviewsRequest = reviewsService.fetchReviews(for: params)
        pushReviews(reviewsRequest)
        //lpad next page
        loadNextPageAction.executionObservables.subscribe(self._reviewResults.asObserver()).disposed(by: disposeBag)
    }
    
    //init
    private let reviewsService: ReviewsServiceType
    private let activity: ActivityType
    required init(dependencies: Dependencies,activity: ActivityType,composeAction: CocoaAction){
        reviewsService = dependencies.reviewsService
        compose = composeAction
        self.activity = activity
        setupRx()
    }
    
   
}



//
//  ReviewSection.swift
//  GetYourGuideReview
//
//  Created by Chris on 19.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import UIKit
import RxDataSources


enum ReviewsSectionModel{
    case activity(activity: ActivityType)
    case reviews(items: [ReviewsSectionItem])
    case loadingIndicator
}

enum ReviewsSectionItem{
    case activity(activity: ActivityType)
    case review(review: Review)
    case loadingIndicator
}

extension ReviewsSectionItem: IdentifiableType, Equatable{
    var identity: Int {
        switch  self {
        case .review(let review):
            return review.reviewId
        case .loadingIndicator:
            return 0
        case .activity:
            return 0
        }
    }
    static func ==(lhs: ReviewsSectionItem, rhs: ReviewsSectionItem) -> Bool {
        return lhs.identity == rhs.identity
    }
}

extension ReviewsSectionModel: IdentifiableType{
    var identity: Int {
        switch self {
        case .reviews(_):
            return 0
        case .loadingIndicator:
            return 1
        case .activity(_):
            return 2
        }
    }
}


extension ReviewsSectionItem{
    var reuseIdentifier: String {
        switch self {
        case .review(_):
            return "reviewCell"
        case .loadingIndicator:
            return "loadingIndicator"
        case .activity:
            return "activityCell"
        }
    }
}

extension ReviewsSectionModel: AnimatableSectionModelType{
    var items: [ReviewsSectionItem] {
        switch self {
            case .reviews(let items):
                return items
            case .loadingIndicator:
                return [ReviewsSectionItem.loadingIndicator]
        case .activity(let activity):
            return [ReviewsSectionItem.activity(activity: activity)]
        }
    }
    
    init(original: ReviewsSectionModel, items: [ReviewsSectionItem]) {
        switch original {
        case .loadingIndicator:
            self = .loadingIndicator
        case .reviews(let items):
            self = .reviews(items: items)
        case .activity(let activity):
            self = .activity(activity: activity)
        }
    }
    
    typealias Item = ReviewsSectionItem
}



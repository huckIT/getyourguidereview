//
//  Scene.swift
//  GetYourGuideReview
//
//  Created by Chris on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation

enum Scene {
    case reviews(viewModel: ReviewsViewModel)
    case composeReview(viewModel: ComposeReviewViewModel)
}

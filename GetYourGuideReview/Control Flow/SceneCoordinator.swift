//
//  SceneCoordinator.swift
//  GetYourGuideReview
//
//  Created by Chris on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import Action

enum TransitionStyle {
    case root
    case push
    case modal
}
public class Coordinator{
    
    let dependencies = AppDependencies()
    let disposeBag = DisposeBag()
    let window: UIWindow
    var currentViewController: UIViewController! = nil
    
    public  required init(window: UIWindow) {
        self.window = window
        guard let viewController = window.rootViewController else { return }
        currentViewController = viewController
    }
    
    lazy var dismissCurrentModal = CocoaAction { [weak self] _ in
        return Observable<Void>.create { subscriber in
            let presenting = self?.currentViewController.presentingViewController
            self?.currentViewController.dismiss(animated: true){
                subscriber.onNext(())
                subscriber.onCompleted()
                self?.currentViewController = presenting
            }
            return Disposables.create()
        }
    }
    
    func transition(to scene: Scene, style: TransitionStyle) -> Observable<Void>{
        //root transition, TODO: implement other types
        return Observable.create{ [weak self] subscriber in
            guard let this = self else { subscriber.onCompleted() ; return Disposables.create()}
            do{
                let viewController = try scene.viewController()
                switch style{
                case .root:
                    this.window.rootViewController = viewController
                    subscriber.onNext(())
                    break
                case .push:
                    this.window.rootViewController = viewController.navigationController
                    subscriber.onNext(())
                    break
                case .modal:
                    this.currentViewController.present(viewController.navigationController!, animated: true) {
                        subscriber.onNext(())
                    }                    
                    break
                }
                this.currentViewController = viewController
            }
            catch{
                #if DEBUG
                    fatalError("Fatal Error due to : \(error.localizedDescription)")
                #endif
                subscriber.onError(error)
            }
            return Disposables.create()
            }.take(1)
        //scene view
    }
    
    public static func actualViewController(for inputViewController: UIViewController) -> UIViewController {
        let viewController: UIViewController
        if let navigationController = inputViewController as? UINavigationController {
            viewController = navigationController.viewControllers.first!
        } else {
            viewController = inputViewController
        }
        return viewController
    }
    
}

//
//  SceneCoordinator+EntryPoints.swift
//  GetYourGuideReview
//
//  Created by Chris on 19.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import UIKit
import Action
import RxSwift



extension Coordinator{
    //when the App starts without previous state
    
    
    func startup(){
    
        let activity = TempelhofActivity()
        
        let composeReview = CocoaAction{ [weak self] _ in
            guard let this = self else {return .empty()}
            //this would result from a higher context in a real-app scenario
            let composeViewModel = ComposeReviewViewModel(dependencies: this.dependencies, activity: activity, dismiss: this.dismissCurrentModal)
            return this.transition(to: Scene.composeReview(viewModel: composeViewModel), style: .modal)
        }
        
        let reviewsViewModel = ReviewsViewModel(dependencies: dependencies, activity: activity, composeAction: composeReview)
        self.transition(to: .reviews(viewModel: reviewsViewModel), style: .push).subscribe(onNext:{ _ in
            #if DEBUG
                print("Startup succeeded!")
            #endif
        }).disposed(by: disposeBag)
    }
    
}

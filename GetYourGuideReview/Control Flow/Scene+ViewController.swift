//
//  Scene+ViewController.swift
//  GetYourGuideReview
//
//  Created by Chris on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import UIKit



enum SceneCoordinatorError: Error{
    case MissingViewController
    case ViewControllerTypeError
}
extension Scene{
    
    func viewController() throws -> UIViewController {
        let storyboard = UIStoryboard.main
        
        let viewController  =  storyboard.instantiateViewController(withIdentifier: self.identifier) 
        
        let actualViewController = Coordinator.actualViewController(for: viewController)
        
        
        switch self{
        case .reviews(let viewModel):
            guard var reviewsViewController  =  actualViewController as? ReviewsViewController else {
                throw SceneCoordinatorError.ViewControllerTypeError
            }
            reviewsViewController.bindViewModelTo(viewModel)
            return reviewsViewController
        case .composeReview(let viewModel):
            guard var composeviewController  =  actualViewController as? ComposeReviewViewController else {
                throw SceneCoordinatorError.ViewControllerTypeError
            }
            composeviewController.bindViewModelTo(viewModel)
            return composeviewController
        }
    }
    
    
    var identifier: String{
        switch self{
        case .reviews(_):
            return "reviews"
        case .composeReview(_):
            return "composeReview"
        }
    }
}

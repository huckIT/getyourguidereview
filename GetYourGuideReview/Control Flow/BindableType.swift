//
//  BindableType.swift
//  GetYourGuideReview
//
//  Created by Chris on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import UIKit

protocol BindableType {
    associatedtype ViewModelType
    var viewModel: ViewModelType! {get set}
    func bindViewModel()
}


extension BindableType where Self: UIViewController{
    
    mutating func bindViewModelTo(_ model: Self.ViewModelType){
        viewModel = model
        loadViewIfNeeded()
        bindViewModel()
    }
}



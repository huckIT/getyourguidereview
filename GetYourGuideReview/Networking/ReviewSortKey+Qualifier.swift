//
//  ReviewOrder+Qualifier.swift
//  GetYourGuideReview
//
//  Created by Christian Huck on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation

enum ReviewSortKey: String {
    case byDate = "date_of_review"
}


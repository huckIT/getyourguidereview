//
//  APIRequest.swift
//  GetYourGuideReview
//
//  Created by Christian Huck on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation



protocol APIRequest{
    var urlParams: [String:String] {get}
}

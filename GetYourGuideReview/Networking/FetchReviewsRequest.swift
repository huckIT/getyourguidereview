//
//  FetchReviewsRequest.swift
//  GetYourGuideReview
//
//  Created by Christian Huck on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation


struct FetchReviewsRequest{
    //Fetching
    let count: Int
    var page: Int
    //Filtering
    let activityId: String
    let travelerType: TravelerType
    let rating: Float?
    //Sorting
    let sortKey: ReviewSortKey
    let sortDirection: SortDirection
    
    
    init(activityId: String, count: Int = 10, travelerType: TravelerType = .unspecified, rating: Float? = nil, sortKey: ReviewSortKey = .byDate, sortDirection: SortDirection = .descending){
        self.activityId = activityId
        self.count = count
        self.travelerType = travelerType
        self.rating = rating
        self.sortKey = sortKey
        self.sortDirection = sortDirection
        self.page = 0
    }
    
    init?(nextPageFor originalResult: FetchReviewsResult){
        guard let nextPage = originalResult.nextPage else {
            return nil
        }
        let original = originalResult.request
        activityId = original.activityId
        count = original.count
        travelerType = original.travelerType
        rating = original.rating
        sortKey = original.sortKey
        sortDirection = original.sortDirection
        page = nextPage
    }
}

extension FetchReviewsRequest {
    var filterPredicate: NSPredicate {
        //Compulsory
        let activityIdPredicate = NSPredicate(format: "activityId = %@", activityId)
        
        var predicates : [NSPredicate] = [activityIdPredicate]
        
        //optional
        if travelerType != .unspecified {
            predicates.append(NSPredicate(format: "travelerTypeId", travelerType.rawValue))
        }
        
        if let rating = self.rating {
            predicates.append(NSPredicate(format: "rating = %f", rating))
        }
        
        return NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
    }
}

extension FetchReviewsRequest: APIRequest{
    //?count=5&page=0&rating=0&type=&sortBy=date_of_review&direction=DESC
    var urlParams: [String : String] {
        return [
            "count": String(count),
            "page": String(page),
            "rating": (rating != nil) ? String(rating!) : "",
            "type": travelerType.rawValue,
            "sortBy": sortKey.rawValue,
            "direction": sortDirection.rawValue
        ]
    }
}

//
//  PostReviewResponse.swift
//  GetYourGuideReview
//
//  Created by Chris on 20.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation

struct PostReviewResponse: Codable {
    let reviewId: Int
    let status: Bool
    
    enum CodingKeys: String, CodingKey {
        case reviewId = "review_id"
        case status
    }
    
}


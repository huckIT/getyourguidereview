//
//  ReviewsResponse.swift
//  GetYourGuideReview
//
//  Created by Chris on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import RxDataSources
import RxSwift


struct ReviewResponse: Codable{
    let status: Bool
    let totalReviews: Int
    let items: [Review]
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
        case items = "data"
        case totalReviews = "total_reviews_comments"
    }
}
extension ObservableType where E == FetchReviewsResult {
    func cache(_ cache: ReviewCacheType) ->  Observable<FetchReviewsResult> {
        return self.do(onNext: { result in
          try cache.cacheResponse(reviews: result.reviews  , activityId: result.request.activityId)
        })
    }
}




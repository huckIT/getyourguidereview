//
//  ActivityService.swift
//  GetYourGuideReview
//
//  Created by Christian Huck on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import Moya

protocol HasReviewsAPI {
    var reviewsAPI: MoyaProvider<ReviewsAPI> {get}
}


enum ReviewsAPI {
    case fetchReviews(request: FetchReviewsRequest)
    case postReview(review: Review,activityId: String)
}

extension ReviewsAPI {
    var decoder: JSONDecoder{
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .formatted(DateFormatter.iso8601Extended)
        return decoder
    }
    var encoder: JSONEncoder{
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .formatted(DateFormatter.iso8601Extended)
        return encoder
    }
}

extension ReviewsAPI: TargetType {
    var baseURL: URL { return URL(string: "https://www.getyourguide.com/berlin-l17")! }
    
    var path: String {
        switch self {
        case .fetchReviews(let request):
            return "\(request.activityId)/reviews.json"
        case .postReview(_,let activityId):
            return "\(activityId)/reviews.json"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .fetchReviews(_):
            return .get
        case .postReview(_):
            return .post
        }
    }
    
    var task: Task {
        switch self {
        case .fetchReviews(let request):
            return .requestParameters(parameters: request.urlParams, encoding: URLEncoding.queryString)
        case .postReview(let review,_):
            return .requestJSONEncodable(review)
        }
    }
    
    var sampleData: Data {
        switch self {
        case .fetchReviews(_):
            return MockResponse.reviews.utf8Encoded
        case .postReview(_):
            return MockResponse.postReview.utf8Encoded
        }
    }
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}
// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}

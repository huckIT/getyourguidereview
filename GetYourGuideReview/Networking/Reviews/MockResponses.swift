//
//  MockResponses.swift
//  GetYourGuideReview
//
//  Created by Chris on 19.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import UIKit

//Mock for the Example Project
struct TempelhofActivity: ActivityType{
    var activityId: String = "tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776"
    
    var title: String = "Tempelhof - 2 hour Airport history tour (Berlin)"
    
    var coverImage: UIImage {
        return UIImage(named:"tour_cover")!
    }
    
    
}

struct MockResponse {
    static let reviews =   "{\"status\":true,\"total_reviews_comments\":10,\"data\":[{\"review_id\":2840117,\"rating\":\"2.0\",\"title\":\"Super informativ\",\"message\":\"Sehr nette und kompetente F\\u00fchrung.\",\"author\":\"Kerstin \\u2013 Germany\",\"foreignLanguage\":true,\"date\":\"June 17, 2018\",\"date_unformatted\":{},\"languageCode\":\"de\",\"traveler_type\":\"friends\",\"reviewerName\":\"Kerstin\",\"reviewerCountry\":\"Germany\"},{\"review_id\":2837792,\"rating\":\"5.0\",\"title\":\"Kompetente und kurzweilige F\\u00fchrung mit Andreas und vielen Infos\",\"message\":\"\",\"author\":\"a GetYourGuide Customer \\u2013 Germany\",\"foreignLanguage\":true,\"date\":\"June 17, 2018\",\"date_unformatted\":{},\"languageCode\":\"de\",\"traveler_type\":\"solo\",\"reviewerName\":\"Rebecca\",\"reviewerCountry\":\"Germany\"},{\"review_id\":2837249,\"rating\":\"5.0\",\"title\":\"Super interessant und kurzweilig.\",\"message\":\"\",\"author\":\"Stefan \\u2013 Germany\",\"foreignLanguage\":true,\"date\":\"June 17, 2018\",\"date_unformatted\":{},\"languageCode\":\"de\",\"traveler_type\":null,\"reviewerName\":\"Stefan\",\"reviewerCountry\":\"Germany\"},{\"review_id\":2800542,\"rating\":\"4.0\",\"title\":\"Interesting but short\",\"message\":\"\",\"author\":\"Ameyalli \\u2013 Germany\",\"foreignLanguage\":false,\"date\":\"June 14, 2018\",\"date_unformatted\":{},\"languageCode\":\"en\",\"traveler_type\":null,\"reviewerName\":\"Ameyalli\",\"reviewerCountry\":\"Germany\"},{\"review_id\":2793819,\"rating\":\"5.0\",\"title\":\"Sehr interessant, wir hatten einen super Guide.\",\"message\":\"Auf Empfehlung haben wir an dieser Tour teilgenommen. Es hat unsere Erwartungen \\u00fcbertroffen. Sehr angenehm war die gr\\u00f6\\u00dfte der Gruppe, dadurch bekam man viel geboren. Die zweite angebotene Tour steht schon auf dem Plan.\",\"author\":\"Sabine \\u2013 Germany\",\"foreignLanguage\":true,\"date\":\"June 13, 2018\",\"date_unformatted\":{},\"languageCode\":\"de\",\"traveler_type\":\"couple\",\"reviewerName\":\"Sabine\",\"reviewerCountry\":\"Germany\"}]}"
    
    static let postReview =   "{\"status\":true,\"review_id\":2900000}"
    
}

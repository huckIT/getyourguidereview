//
//  PersistenceService.swift
//  GetYourGuideReview
//
//  Created by Chris on 19.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import RealmSwift

protocol PersistenceServiceType {
    func write<R>(_ operation: (Realm) -> R ) throws -> R
    func read<R>(_ operation: (Realm) -> R )  throws -> R
}

protocol HasPersistenceService {
    var persistenceService: PersistenceServiceType {get}
}
enum PersistenceServiceError: Error{
    case RealmReadFailed(Realm.Error)
    case RealmWriteFailed(Realm.Error)
}
struct PersistenceService: PersistenceServiceType {
    
    func write<R>(_ operation: (Realm) -> R) throws -> R {
        return try executeOperation(write: true, operation: operation)
    }
    
    func read<R>(_ operation: (Realm) -> R) throws -> R {
        return try executeOperation(write: false, operation: operation)
    }
    
    private func executeOperation<R>(write: Bool,  operation: (Realm) -> R) throws -> R {
        do{
            let realm = try _realmProvider.realm()
            let result: R
            if (write){  realm.beginWrite() }
            result = operation(realm)
            if (write){ try realm.commitWrite() }
            return result
        }
        catch let error as Realm.Error {
            throw (write) ? PersistenceServiceError.RealmWriteFailed(error) : PersistenceServiceError.RealmReadFailed(error)
        }
    }
    
    private let _realmProvider: RealmProviderType
    
    init(realmProvider: RealmProviderType){
        _realmProvider = realmProvider
    }
}

//
//  AppDependencies.swift
//  GetYourGuideReview
//
//  Created by Chris on 19.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import Moya
import Reachability
import RxReachability

struct InternalDependencies: HasPersistenceService,HasReviewsAPI,HasReviewsCache, HasReachability {
    
    let persistenceService: PersistenceServiceType
    let reviewsAPI: MoyaProvider<ReviewsAPI>
    let reviewsCache: ReviewCacheType
    let reachability: ReachabilityType
}

struct AppDependencies: HasPersistenceService, HasReviewsService,HasUserProvider {
    
    var persistenceService: PersistenceServiceType
    
    var reviewsService: ReviewsServiceType
    
    var userProvider: UserProviderType
    
    private let internalDependencies: InternalDependencies
    
    init(){
        let realmProvider = RealmProvider()
        //Internal dependencies
        persistenceService = PersistenceService(realmProvider: realmProvider)
        
        #if DEBUG
            let plugins: [PluginType] = [NetworkLoggerPlugin()]
        #else
            let plugins: [PluginType] = []
        #endif
        
        let moyaAPI =  MoyaProvider<ReviewsAPI>.init( stubClosure: { server -> StubBehavior in
            //TODO: for testing
            return StubBehavior.never
        },plugins: plugins)
        
        let cache = ReviewsCache(persistenceService: persistenceService)
        
        userProvider = UserProviderMock()
        
        //TODO: research how ! can be avoided
        let reachability = ReachabilityService(reachability:Reachability()!)
        
        internalDependencies = InternalDependencies(persistenceService: persistenceService, reviewsAPI: moyaAPI, reviewsCache: cache, reachability:reachability)
        
        //Global App Dependencies
        reviewsService = ReviewsService(dependencies: internalDependencies)
    }
}

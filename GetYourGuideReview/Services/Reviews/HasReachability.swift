//
//  HasReachability.swift
//  GetYourGuideReview
//
//  Created by Christian Huck on 20.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import Reachability
import RxReachability
import RxSwift


protocol ReachabilityType{
    var status: Observable<Reachability.Connection> {get}
}

struct ReachabilityService{
    let reachability: Reachability
    init(reachability: Reachability){
        self.reachability = reachability
        try? reachability.startNotifier()
    }
}

extension ReachabilityService: ReachabilityType {
    var status: Observable<Reachability.Connection> {
        return self.reachability.rx.status
    }
}

protocol HasReachability{
    var reachability: ReachabilityType {get}
}

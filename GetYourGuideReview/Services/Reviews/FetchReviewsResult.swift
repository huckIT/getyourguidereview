//
//  FetchReviewsResult.swift
//  GetYourGuideReview
//
//  Created by Chris on 20.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation


struct FetchReviewsResult {
    
    var reviews: [Review] {
        return  _previousReviews + (response?.items ?? [])
    }
    
    var currentPage: Int{
        return request.page
    }
    
    private let _previousReviews: [Review]
    
    var pageCount: Int{
        guard let response = self.response else { return 1}
        let total = response.totalReviews
        let resultsPerPage = request.count
        let pageCount = ceil(Double(total)/Double(resultsPerPage))
        return  Int(pageCount)
    }
    
    var nextPage: Int?{
        let nextPage = currentPage + 1
        if currentPage + 1 >= pageCount { return nil}
        return nextPage
    }
    
    
    let response: ReviewResponse?
    private(set) var request: FetchReviewsRequest
    
    
    
    init(request: FetchReviewsRequest, reviews: [Review]){
        self.request = request
        self.response = nil
        _previousReviews = reviews
    }
    
    init(request: FetchReviewsRequest, response: ReviewResponse){
        self.request = request
        self.response = response
        _previousReviews = []
    }
    
    init(previousResult: FetchReviewsResult,params: FetchReviewsRequest, response: ReviewResponse){
        _previousReviews = previousResult.reviews
        self.response = response
        request = params
    }
}


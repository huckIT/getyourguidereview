//
//  ReviewService.swift
//  GetYourGuideReview
//
//  Created by Christian Huck on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift
import RxSwiftExt
import RxRealm
import Moya
import Reachability
import RxReachability


protocol ReviewsServiceType {
    func fetchReviews(for params: FetchReviewsRequest) -> Observable<FetchReviewsResult>
    func fetchNextPage(for previousResult: FetchReviewsResult) -> Observable<FetchReviewsResult>
    func publish(review: Review, for activityId: String) -> Observable<PublishReviewResult>
}

protocol HasReviewsService {
    var reviewsService: ReviewsServiceType {get}
}


struct ReviewsService: ReviewsServiceType,Dependent{
    
    typealias Dependencies  = HasPersistenceService & HasReviewsAPI & HasReviewsCache & HasReachability
    
    
    func fetchNextPage(for previousResult: FetchReviewsResult) -> Observable<FetchReviewsResult>{
        guard let params = FetchReviewsRequest(nextPageFor: previousResult) else {
            //TODO: error
            return Observable.empty()
        }
        return _loadReviewsFromAPI(for: params)
            .map{ response in
                return FetchReviewsResult(previousResult: previousResult, params: params, response: response)
            }.cache(self.cache)
    }
    
    private func _loadReviewsFromAPI(for params: FetchReviewsRequest) -> Observable<ReviewResponse>{
        //load from web
        let request = ReviewsAPI.fetchReviews(request: params)
        let response: Observable<ReviewResponse> =  api.rx.request(request)
            .asObservable()
            .mapDecodable(ReviewResponse.self,jsonDecoder: request.decoder )
            .catchError { (error) -> Observable<ReviewResponse> in
                return .empty()
        }
        return response
    }
    
    private func _loadReviewsFromCache(for params: FetchReviewsRequest) throws -> Observable<FetchReviewsResult>{
        //load from cache
        do{
            let reviews = try self.cache.fetchReviews(params: params)
            let result = FetchReviewsResult(request: params, reviews: reviews)
            return Observable.just(result)
        }
        catch{
            return Observable.error(error)
        }
    }
    
    
    func fetchReviews(for params: FetchReviewsRequest) -> Observable<FetchReviewsResult>{
        //ceck if online
        return isOnline.distinctUntilChanged()
            .map{ online -> Observable<FetchReviewsResult> in
                if online{
                    return self._loadReviewsFromAPI(for: params).map{  response  in
                        return FetchReviewsResult(request: params, response: response)
                        }.cache(self.cache)
                }
                else{
                    return try self._loadReviewsFromCache(for: params)
                }
            }
            .switchLatest()
    }
    
    
    func publish(review: Review, for activityId: String) -> Observable<PublishReviewResult> {
        return api.rx.request(.postReview(review: review, activityId: activityId))
            .asObservable().mapDecodable(PostReviewResponse.self)
            .unwrap()
            .map{ (response: PostReviewResponse) in
                return response.status
        }
    }
    
    
    let isOnline = BehaviorSubject<Bool>(value: false)
    
    //RX
    let disposeBag = DisposeBag()
    
    func setupRX(){
        self.reachability.status
            .map{ status in
                switch status{
                case .none:
                    return false
                default:
                    return true
                }
            }.subscribe(self.isOnline).disposed(by: disposeBag)
    }
    
    let persistence: PersistenceServiceType
    let api: MoyaProvider<ReviewsAPI>
    let cache: ReviewCacheType
    let reachability: ReachabilityType
    
    init(dependencies: Dependencies){
        self.persistence = dependencies.persistenceService
        self.api = dependencies.reviewsAPI
        self.cache = dependencies.reviewsCache
        self.reachability = dependencies.reachability
        
        setupRX()
    }
    
    
}






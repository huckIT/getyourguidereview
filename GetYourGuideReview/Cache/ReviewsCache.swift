//
//  ReviewsCache.swift
//  GetYourGuideReview
//
//  Created by Chris on 19.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import RealmSwift
import RxSwift
import RxSwiftExt
import RxRealm
import Moya

protocol ReviewCacheType {
    func cacheResponse(reviews: [ReviewType],activityId: String) throws
    func fetchReviews(params: FetchReviewsRequest) throws -> [Review]
}

enum ReviewsCacheError: Error{
    case CachingFailed(Error)
}

protocol  HasReviewsCache{
    var reviewsCache: ReviewCacheType {get}
}

struct ReviewsCache:  ReviewCacheType{
    
    typealias Dependencies  = HasPersistenceService
    let persistenceService: PersistenceServiceType
    
    func fetchReviews(params: FetchReviewsRequest)throws -> [Review] {
        let reviews = try persistenceService.read { realm  in
            return realm.objects(ReviewModel.self)
                .filter(params.filterPredicate)
                .sorted(byKeyPath: params.sortKey.rawValue, ascending: (params.sortDirection == .ascending))
        }
        return reviews
            .toArray().map{ Review($0) }
    }
    
    func cacheResponse(reviews: [ReviewType],activityId: String) throws{
        do{
            try persistenceService.write { realm  in
                let models = reviews.map{ReviewModel(review: $0, activityId: activityId)}
                for model in models {
                    realm.add(model,update: true)
                }
            }
        }
        catch{
            throw ReviewsCacheError.CachingFailed(error)
        }
    }
    
    
    init(persistenceService: PersistenceServiceType){
        self.persistenceService = persistenceService
    }
}

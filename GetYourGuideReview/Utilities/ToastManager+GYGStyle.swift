//
//  ToastManager+GYGStyle.swift
//  GetYourGuideReview
//
//  Created by Chris on 20.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import UIKit
import Toast_Swift

enum Toasty{
    case warn(message: String)
    case error(message: String)
    case positive(message: String)
    
    var message: String {
        switch  self {
        case .warn(let message), .positive(let message),.error(let message):
            return message
        }
    }
    var style: ToastStyle{
        var style = ToastStyle()
        switch self {
        case .positive(_):
            style.backgroundColor = UIColor(red: 0.012, green: 0.633, blue: 0.285, alpha: 1.0)
            style.messageColor = .white
            return style
        case .warn(_),.error(_):
            style.backgroundColor = UIColor(named:"GetYourGuide")!
            style.messageColor = .white
            return style
        }
    }
    
    var title: String? {
        switch self {
        case .positive(_),.warn(_):
            return nil
        case .error(_):
            return "We are sorry, something wen't wrong!"
        }
    }
    
    var duration: TimeInterval{
        switch self{
        case .positive(_),.warn(_),.error(_):
            return 3.0
        }
    }
    
    var position: ToastPosition{
        switch self {
        case .positive(_),.warn(_),.error(_):
            return .center
        }
    }
}

extension UIView{
    func makeToasty(_ toasty: Toasty){
        self.makeToast(toasty.message, duration: toasty.duration, position: toasty.position, title: toasty.title, style: toasty.style)
    }
}
extension ToastManager {
    static func configureGYGStyle(){
        // create a new style
        var style = ToastStyle()
        
        // this is just one of many style options
        style.backgroundColor = UIColor(named:"GetYourGuide")!
        style.messageColor = .white
        
        ToastManager.shared.style = style
        
        ToastManager.shared.isTapToDismissEnabled = true
        
        // toggle queueing behavior
        ToastManager.shared.isQueueEnabled = false
    }
}

//
//  DateFormatter+ISO8601Extended.swift
//  GetYourGuideReview
//
//  Created by Chris on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import UIKit

extension DateFormatter {
    static let iso8601Extended: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeZone = .none
        formatter.locale = Locale(identifier: "en_US")        
        return formatter
    }()
}

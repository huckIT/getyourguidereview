//
//  UIView+Rx+Toasts.swift
//  GetYourGuideReview
//
//  Created by Chris on 20.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import Toast_Swift
extension Reactive where Base == UIView{
     var toasts: Binder<Toasty> {
        return Binder(self.base) { view, toasty in
                view.makeToasty(toasty)
            }
        }
}

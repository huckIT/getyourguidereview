//
//  Dependent.swift
//  GetYourGuideReview
//
//  Created by Chris on 19.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation


protocol Dependent {
    associatedtype Dependencies    
    init(dependencies: Dependencies)
}

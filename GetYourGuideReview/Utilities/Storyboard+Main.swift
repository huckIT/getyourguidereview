//
//  Storyboard+Main.swift
//  GetYourGuideReview
//
//  Created by Chris on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard{
    static var main: UIStoryboard{
        return UIStoryboard(name: "Main", bundle: nil)
    }
}

//
//  Moya+Decodable.swift
//  GetYourGuideReview
//
//  Created by Chris on 19.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import RxSwift
import Moya

extension ObservableType where Self.E == Moya.Response {
    func mapDecodable<D: Decodable>(_ type: Decodable.Type, jsonDecoder: JSONDecoder? = nil) -> Observable<D>{
        return self.map({ response -> D in
            let decoder =  jsonDecoder ?? JSONDecoder()
            return try decoder.decode(D.self, from: response.data)
        })
    }
}



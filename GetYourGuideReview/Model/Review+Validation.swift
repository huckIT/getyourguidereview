//
//  Review+Validation.swift
//  GetYourGuideReview
//
//  Created by Chris on 20.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import RxSwift


enum ReviewValidationError: Error{
    case NeedsRating
    case TitleTooShort(required: Int)
    case MessageTooShort(required: Int)
    case TitleOrMessageNeeded
}

extension Review{
    func validateForSubmission() -> Observable<Bool>  {
        
        //Rating
        guard let intRating = Int(rating) else { return .error(ReviewValidationError.NeedsRating)}
        if  !(1 ... 5).contains(intRating)    {  return .error(ReviewValidationError.NeedsRating) }        
        if let title = title {
            if title.count < 10 {
                 return .error(ReviewValidationError.TitleTooShort(required: 10-title.count))
            }
        }
        else if message == nil{
             return .error(ReviewValidationError.TitleOrMessageNeeded)
        }
        else{
            if message!.count < 20 {
                 return .error(ReviewValidationError.MessageTooShort(required: 20-message!.count))
            }
        }
        return .just(true)
    }
    //all good
}

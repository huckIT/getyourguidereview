//
//  ReviewType.swift
//  GetYourGuideReview
//
//  Created by Chris on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import RxDataSources

protocol ReviewType: HasTravelerTypeId{
    var reviewId: Int {get}
    var rating: String {get}
    var author: String {get}
    var foreignLanguage: Bool {get}
    var date: Date {get}
    var languageCode: String {get}
    var travelerTypeId: String? {get}
    var reviewerName: String {get}
    var reviewerCountry: String {get}
    var title: String? {get}
    var message: String? {get}
}


extension ReviewType{
    var ratingValue: Float?{
        return Float(rating)
    }
}

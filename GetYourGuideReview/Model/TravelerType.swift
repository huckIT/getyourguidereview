//
//  TravelerType.swift
//  GetYourGuideReview
//
//  Created by Chris on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation


protocol HasTravelerTypeId {
    var travelerTypeId: String? {get}
}

extension HasTravelerTypeId {
    var travelerType: TravelerType {
        guard let travelerTypeId = self.travelerTypeId else { return .unspecified}
        guard let type = TravelerType(rawValue: travelerTypeId) else {
            #if DEBUG
                fatalError("tried to access invalid traveler type")
            #endif
            return .unspecified
        }
        return type
    }
}
enum TravelerType: String{
    case unspecified = ""
    case friends = "friends"
    case couple = "couple"
    //TODO: check if more options available in API
}

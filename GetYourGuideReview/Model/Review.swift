//
//  Review.swift
//  GetYourGuideReview
//
//  Created by Chris on 20.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import RxDataSources

struct Review: Codable, ReviewType, HasTravelerTypeId {
    var reviewId: Int
    let rating: String
    let title: String?
    let message: String?
    let author: String
    let foreignLanguage: Bool
    let date: Date
    let languageCode: String
    let travelerTypeId: String?
    let reviewerName: String
    let reviewerCountry: String
    
    enum CodingKeys: String, CodingKey {
        case reviewId = "review_id"
        case travelerTypeId = "traveler_type"
        case rating,title,message,author,foreignLanguage,date,languageCode,reviewerName,reviewerCountry
    }
}

extension Review{
    init(_ reviewType: ReviewType){
        reviewId = reviewType.reviewId
        rating = reviewType.rating
        title = reviewType.title
        message = reviewType.message
        author = reviewType.author
        foreignLanguage = reviewType.foreignLanguage
        date = reviewType.date
        languageCode = reviewType.languageCode
        travelerTypeId = reviewType.reviewerName
        reviewerName = reviewType.reviewerName
        reviewerCountry = reviewType.reviewerCountry
        
    }
}
extension Review: IdentifiableType{
    var identity: Int {
        return reviewId
    }
}

extension Review: Equatable{
    static func ==(lhs: Review, rhs: Review) -> Bool {
        return lhs.identity == rhs.identity
    }
}

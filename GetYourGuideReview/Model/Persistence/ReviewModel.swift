//
//  Review.swift
//  GetYourGuideReview
//
//  Created by Christian Huck on 18.06.18.
//  Copyright © 2018 getyourguide. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class ReviewModel: Object, ReviewType {
    dynamic var reviewId: Int = 0
    //In a real world scenario this is a reverse relationship 
    dynamic var activityId: String = ""
    dynamic var rating: String = ""
    dynamic var author: String = ""
    dynamic var foreignLanguage: Bool = false
    dynamic var date_of_review: Date = Date()
    dynamic var languageCode: String = "en"
    @objc dynamic var travelerTypeId: String? = TravelerType.unspecified.rawValue
    dynamic var reviewerName: String = ""
    dynamic var reviewerCountry: String = ""
    dynamic var title: String? = ""
    dynamic var message: String? = ""
    
    
    override class func primaryKey() -> String? {
        return "reviewId"
    }
    
    convenience init(review: ReviewType,activityId: String){
        self.init()
        reviewId = review.reviewId
        self.activityId = activityId
        title  = review.title
        message  = review.message
        rating = review.rating
        author = review.author
        foreignLanguage = review.foreignLanguage
        date = review.date
        languageCode = review.languageCode
        travelerTypeId = review.travelerTypeId
        reviewerName = review.reviewerName
        reviewerCountry = review.reviewerCountry
    }
}

extension ReviewModel{
    var date: Date {
        get{
            return date_of_review
        }
        set{
            date_of_review = newValue
        }
    }
}




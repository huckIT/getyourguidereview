## Setup

After downloading the project, it can be installed using CocoaPods:

   pod install 
   
## Known Issues

- Unit Tests not building: Due to recently added frameworks unit test build breaks (fixed)
- Unit Tests are incomplete (time constraints)
- Storyboard agent crashes sometimes (due to embedded XIB)
- One Autolayout constraint issue in storyboard 

## Missing / Partially implemented features
- Sorting and filtering is left out in the UI. Full functionality is available though in the corresponding services

